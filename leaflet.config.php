<?php
/**
 * Leaflet MODX configuration file
 *
 * Change settings as required (take extra care with quotes)
 */
class MapConfig
{

    /**
     * Constants
     */
    const MAP_PROVIDER = 'MapQuest',  // Select from tileProviders array
          MAP_STYLE    = 'leaflet',   // Stylesheet file prefix
          MAP_DATA     = 'USGS',      // Select from dataSources array (empty for no data)
          MAP_SCROLL   = true,        // Allow mouse wheel scroll to zoom
          MAP_DEBUG    = false;       // Output error messages to browser & console
}

/**
 * Tile providers
 *
 * Set MAP_PROVIDER constant to one of the array keys
 */
$tileProviders = array(

    'MapQuest' => array(
        'map_tiles'     => 'MQOSM',
        'map_url'       => 'http://{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png',
        'map_options'   => '{
                            maxZoom: 16,
                            minZoom: 2,
                            subdomains: ["otile1", "otile2", "otile3", "otile4"],
                            attribution: \'<a href="http://www.mapquest.com" target="_blank">MapQuest</a> | <a href="http://www.openstreetmap.org" target="_blank">OpenStreetMap contributors</a> | <a href="http://www.easystreetasia.com/" target="_blank">Easy Street Asia</a>\'
                        }'
    ),

    'OpenStreetMap' => array(
        'map_tiles'     => 'OSM',
        'map_url'       => 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        'map_options'   => '{
                            maxZoom: 18,
                            minZoom: 2,
                            attribution: \'<a href="http://www.openstreetmap.org" target="_blank">OpenStreetMap contributors</a> | <a href="http://www.easystreetasia.com/" target="_blank">Easy Street Asia</a>\'
                        }'
    )

);

/**
 * GeoJSON data sources
 *
 * Set MAP_DATA constant to one of the array keys
 */
$dataSources = array(

    'USGS' => array(
        'data_url'      =>  'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_month.geojson',
        'data_title'    =>  'USGS M5+ earthquakes for the last 30 days',
        'data_filter'   =>  'feature.properties.mag > 5',
        'data_circle'   =>  '{color: "red", fillOpacity: 0.5, radius: feature.properties.mag*2.5}',
        'props_title'   =>  'properties.title',
        'props_text'    =>  'properties.type',
        'props_link'    =>  'properties.url'
    ),

    'QLDcams' => array(
        'data_url'      =>  'http://131940.qld.gov.au/api/json/v1/poi/general/webcams',
        'data_title'    =>  'Queensland Australia traffic cameras',
        'data_filter'   =>  '',
        'data_circle'   =>  '{fillOpacity: 0.75, radius: 10}',
        'props_title'   =>  'id',
        'props_text'    =>  'properties.location.suburb',
        'props_link'    =>  'properties.metadata.url'
    )

);
