<?php
/**
 * Leaflet MODX Snippet
 * @author Mark Stoneman <upstyx@gmail.com>
 * @license LGPL
 */
$path = MODX_CORE_PATH . 'components/leaflet-modx/';

/**
 * Initialize settings & generate HTML
 */
require ($path . 'leaflet.class.php');
$map = new LeafletMap($modx);

try {

    $map->loadConfig($path, $config);
    $map->getSettings();
    $html = $map->loadAssets();

} catch (Exception $e) {

    if (MapConfig::MAP_DEBUG) {
        return $e->getMessage();
    } else {
        $modx->log(modX::LOG_LEVEL_ERROR, $e->getMessage());
	return false;
    }

}

return $html;
