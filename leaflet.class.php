<?php
/**
 * Leaflet MODX controller Class
 * @author Mark Stoneman <upstyx@gmail.com>
 * @license LGPL
 */
Class LeafletMap
{

    public function __construct($modx)
    {
        $this->modx = &$modx;
    }

	/**
	 * Load settings from configuration file
	 *
	 * @param string $path Path to add-on files
	 * @param string $config Custom config file from snippet call
	 * @throws Exception Config file not found
	 */
    public function loadConfig($path, $config)
    {

        if ($config == '') {
            $configFile = $path .'leaflet.config.php';
        } else {
            $configFile = $path . $config . '.config.php';
        }

        if (!file_exists($configFile)) {
            throw new Exception('Fatal error: config file not found: ' . $configFile);
        }

        require ($configFile);

        $this->path = $path;
        $this->tileProviders = $tileProviders;
        $this->dataSources = $dataSources;

    }

	/**
	 * Prepare & combine all settings: constants, tileProviders & dataSources
	 * Inject <link> in <head> for remote geodata if MAP_DATA constant is specified
	 * 
	 * @throws Exception Specified provider config not found
	 * @throws Exception Specified data source config not found
	 */
    public function getSettings()
    {

        $aSettings = $this->tileProviders[MapConfig::MAP_PROVIDER];
        if (empty($aSettings)) {
            throw new Exception('Error: config not found for provider ' . MapConfig::MAP_PROVIDER);
        }

        $aSettings['map_scroll'] = (int) MapConfig::MAP_SCROLL;
        $aSettings['map_debug'] = (int) MapConfig::MAP_DEBUG;

        if (MapConfig::MAP_DATA != '') {

            $aData = $this->dataSources[MapConfig::MAP_DATA];
            if (empty($aData)) {
                throw new Exception('Error: config not found for data source ' . MapConfig::MAP_DATA);
            }

            $dataUrl = $aData['data_url'];
            $dataLink = '<link rel="geodata" type="application/json" href="' . $dataUrl . '">';
            $this->modx->regClientStartupHTMLBlock($dataLink);

            foreach ($aData as $key => $value) {
                $aSettings[$key] = $value;
            }
            $aSettings['map_data'] = '1';

            $this->settings = $aSettings;

        }

    }

	/**
	 * Inject CSS file and set HTML
	 *
	 * @throws Exception CSS file not found
	 * @throws Exception CSS file is empty
	 * @return string Output HTML
	 */
    public function loadAssets()
    {

        $cssFile = $this->path . 'leaflet.css';
        if (file_exists($cssFile)) {
            $css = file_get_contents($cssFile);
            if ($css == '') {
                throw new Exception('Error: CSS file is empty: ' . $cssFile);
            }
        } else {
            throw new Exception('Error: CSS file not found: ' . $cssFile);
        }

        $this->modx->regClientCSS('//cdn.leafletjs.com/leaflet-0.7.3/leaflet.css');
        $this->modx->regClientStartupHTMLBlock("<style>\n$css\n</style>");
        $this->modx->regClientScript('//cdn.leafletjs.com/leaflet-0.7.3/leaflet.js');
        $this->modx->regClientScript('http://code.jquery.com/jquery-2.1.0.min.js');

        $js = $this->modx->getChunk('LeafletJS', $this->settings);
        $this->modx->regClientHTMLBlock("<script>\n$js\n</script>");

        $html = '';
        $title = $this->settings['data_title'];
        if ($title != '') $html = "<h2>$title</h2>\n";
        $html .= '<div id="map"></div>';
        if (MapConfig::MAP_DEBUG) $html .= "\n<h3>Warning: Debug is ON</h3>";

        return $html;

    }

}
