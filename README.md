# Leaflet MODX #

This add-on for [MODX](http://modx.com/) will display a [Leaflet](http://leafletjs.com/) map on the page where a MODX snippet call is placed.

This is a basic configuration which may serve as a flexible starting point for mapping projects.

[Demo: USGS M5+ earthquakes for the last 30 days:](http://leaflet.easystreet.modxcloud.com/)
![map.jpg](https://bitbucket.org/repo/ryapB7/images/1013026226-map.jpg)

**Quick Start**

* Copy all the files to this folder under your MODX installation: core/components/leaflet-modx.
* Create a snippet called LeafletMap; Tick "Is Static" and make sure "Media Source" is "Filesystem"; Set "Static File" to /core/components/leaflet-modx/leaflet.php.
* Create a chunk called LeafletJS; Tick "Is Static" and make sure "Media Source" is "Filesystem"; Set "Static File" to /core/components/leaflet-modx/leaflet.js.
* Place a snippet call on the page where you want the map to appear: 
```
[[LeafletMap]]
```

Point your browser to the page and you should see a map with markers.

If the map is not displaying, check the MODX Error Log for insight *OR* enable MAP_DEBUG in the config file to get error output on the page and in a JS console.


**More Info**

Files:

* leaflet.php:        Static file for the snippet.
* leaflet.class.php:  Controller class.
* leaflet.config.php: Default configuration file.
* leaflet.css:        Default stylesheet.
* leaflet.js:         Static file for the Javascript chunk.

There is some information about the settings in the config file, but it's all fairly self-explanatory.

This add-on uses [jQuery](https://jquery.com/) for one specific function: getJSON. This is only necessary for the remote GeoJSON data that are being used.

Credit for the concept of using jQuery and link relations for remote GeoJSON goes to [Lyzi Diamond](http://lyzidiamond.com/posts/osgeo-august-meeting/).


**Customising**

If you want to use a different folder, just change the 'path' variable at the top of leaflet.php.

To alter the configuration:

* Copy the default config file and edit the copy as required (eg custom.config.php).
* Call the snippet with the config parameter set to the new filename prefix, for example:
```
[[LeafletMap?config=`custom`]]
```

The default stylesheet is injected into the <head> of the calling document as a <style> block.

To customise the styling:

* Copy the default stylesheet file and edit the copy as required (eg custom.css).
* Set the MAP_STYLE constant in the config file to your filename prefix (eg custom).

Enjoy!