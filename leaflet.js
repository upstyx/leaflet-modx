  /**
  * Leaflet JS
  */

[[+map_debug:is=`1`:then=`
  debug = true;
`:else=`
  debug = false;
`]]

  if (debug) console.log('leaflet.js called by leaflet.php');

  var layer[[+map_tiles]] = L.tileLayer('[[+map_url]]', [[+map_options]]);
  var map = L.map('map', {center: [0,0], layers: layer[[+map_tiles]]});

[[+map_scroll:is=`0`:then=`
  map.scrollWheelZoom.disable();
  if (debug) console.log('scrollWheelZoom.disable()');
`]]

[[+map_data:is=`1`:then=`

  /* Yay, we have data */

  var geourl = $('link[rel="geodata"]').attr("href");

  if (debug) console.log('geourl: ' + geourl);

  $.getJSON(geourl, function(geodata) {

    var geojson = L.geoJson(geodata, {
      onEachFeature: function (feature, layer) {
        var title = feature.[[+props_title]] + '<br>';
        var text = feature.[[+props_text]] + '<br>';
        var link = feature.[[+props_link]];
        if (link != '') link = '<a href="' + link + '" target="_blank">Detail</a>';
        layer.bindPopup(title + text + link);
      },
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, [[+data_circle]]);
      },
    [[+data_filter:isnt=``:then=`
      filter: function(feature) {
        if ([[+data_filter]]) return true;
      }
    `]]
    }).addTo(map);

    if (debug) console.log('data filter: [[+data_filter]]');

    map.fitBounds(geojson.getBounds());
    if (debug) console.log('fitBounds()');

  });

`:else=`

  /* Boring, no data */

  map.fitWorld();
  if (debug) console.log('fitWorld()');

`]]
